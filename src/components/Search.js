import React, {Component} from "react";


export default class Search extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const {actions} = this.props;
    actions.filter(e.target.value)
  }

  render() {
    return (
        <div className="search">
          <input type="text" className="search input" onChange={this.handleChange}/>
        </div>
    );
  }
}