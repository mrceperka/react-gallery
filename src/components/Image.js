import React, {Component} from "react";


export default class Image extends Component {
  render() {
    const {image: {src}, config} = this.props;

    let dataAttributes = {};
    config.image.data.map((o) => {
      dataAttributes[o.name] = o.prefix + this.props.image[o.key] + o.suffix;
    });
    return (
        <div className="image" {...dataAttributes}>
          <img src={src}/>
        </div>
    );
  }
}
