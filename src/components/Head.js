import React, {Component} from "react";
import Pager from "./Pager";
import Search from "./Search";


export default class Head extends Component {
  render() {
    const {translations, config, actions, gallery, gallery: {offset}, gallery: {images}} = this.props;
    return (
        <div className="head">
          <button onClick={() => this.props.actions.fetchImages(config.fetch.url, config.fetch.options)}>
            {gallery.fetch.images.started === true ? translations.messages.fetch.fetching : ""}
            {gallery.fetch.images.succeeded === true ? translations.messages.fetch.success : ""}
            {gallery.fetch.images.failed === true ? translations.messages.fetch.failed : ""}
            {gallery.fetch.images.failed === false && gallery.fetch.images.succeeded === false && gallery.fetch.images.started === false ? translations.button.fetch : ""}
          </button>
          <Pager actions={actions} offset={offset} images={images} translations={translations}/>
          {config.filterEnabled ? <Search actions={actions}/> : ""}
        </div>
    );
  }
}
