import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Images from "./Images";
import FilteredImages from "./FilteredImages";
import Head from "./Head";
import {next, prev, fetchImages, filter} from "../actions/gallery";

class Gallery extends Component {
  componentDidMount() {
    const {actions, config} = this.props;
    if(config.fetch.atStart) {
      actions.fetchImages(config.fetch.url, config.fetch.options);
    }
  }

  render() {
    const {translations, gallery: {images}, config} = this.props;
    return (
        <div>
          <Head {...this.props}/>
          {config.filterEnabled ? <FilteredImages images={images} config={config} translations={translations}/> : ""}
          <Images images={images} config={config} translations={translations}/>
        </div>
    );
  }
}

Gallery.propTypes = {
  gallery: React.PropTypes.shape({
    offset: React.PropTypes.number.isRequired,
    images: React.PropTypes.shape({
      all: React.PropTypes.arrayOf(
          React.PropTypes.shape({
            id: React.PropTypes.number.isRequired,
            name: React.PropTypes.string.isRequired,
            src: React.PropTypes.string.isRequired
          }).isRequired
      ).isRequired
    }).isRequired,
  }).isRequired,
};

export default connect(
    state => ({
      gallery: state.gallery,
    }),
    dispatch => ({
      actions: bindActionCreators({
        next,
        prev,
        filter,
        fetchImages
      }, dispatch)
    })
)(Gallery)
