import React, {Component} from "react";
import {Provider} from "react-redux";
import Gallery from "./Gallery";

require('../theme/styles/main.css')

// If you use React Router, make this component
// render <Router> with your routes. Currently,
// only synchronous routes are hot reloaded, and
// you will see a warning from <Router> on every reload.
// You can ignore this warning. For details, see:
// https://github.com/reactjs/react-router/issues/2182


export default class App extends Component {
  render() {
    const {store, config, translations} = this.props;
    return (
        <Provider store={store}>
          <Gallery config={config} translations={translations}/>
        </Provider>
    );
  }
}

App.propTypes = {
  config: React.PropTypes.shape({
    filterEnabled: React.PropTypes.bool.isRequired,
    image: React.PropTypes.shape({
      data: React.PropTypes.arrayOf(
          React.PropTypes.shape({
            name: React.PropTypes.string.isRequired,
            prefix: React.PropTypes.string.isRequired,
            key: React.PropTypes.string.isRequired,
            suffix: React.PropTypes.string.isRequired,
          })
      )
    }),
    fetch: React.PropTypes.shape({
      url: React.PropTypes.string.isRequired,
      atStart: React.PropTypes.bool.isRequired,
      options: React.PropTypes.object.isRequired
    }),
  }).isRequired
};


