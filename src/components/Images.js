import React, {Component} from "react";
import Image from "./Image";


export default class Images extends Component {
  render() {
    const {translations, images: {current}, config} = this.props;
    return (
        <div>
          <span>{translations.images.def}</span>
          <div className="images">
            {current.map((image) => <Image key={image.id} image={image} config={config}/>)}
          </div>
        </div>
    );
  }
}