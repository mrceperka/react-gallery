import React, {Component} from "react";
import Image from "./Image";


export default class FilteredImages extends Component {
  render() {
    const {translations, images: {filtered}, config} = this.props;
    let result = "";
    if (filtered.length > 0) {
      result = filtered.map((image) => <Image key={'filtered' + image.id} image={image} config={config}/>)
    }
    return (
        <div>
          {result ? <span>{translations.images.filtered}</span> : ''}
          <div className="images filtered">
            {result}
          </div>
        </div>
    );
  }
}