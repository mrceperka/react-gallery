import React, {Component} from "react";


export default class Pager extends Component {
  render() {
    const {translations, actions, offset, images: {all}} = this.props;
    return (
        <div className="pager">
          <button className="prev" onClick={actions.prev} disabled={offset === 6 || offset === 0}>
            {translations.button.prev}
          </button>
          <button className="next" onClick={actions.next} disabled={offset >= all.length || all.length === 0}>
            {translations.button.next}
          </button>
        </div>
    );
  }
}