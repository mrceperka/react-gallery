import * as actions from "../constants/actions";
import {LIMIT} from "../constants";

const initialState = {
  offset: 0,
  images: {
    all: [],
    filtered: [],
    current: [],
  },
  fetch: {
    images: {
      started: false,
      succeeded: false,
      failed: false,
    }
  },
};
const gallery = (state = initialState, action) => {

  switch (action.type) {
    case actions.NEXT:
      if (state.images.all.length === 0) {
        return state;
      }
      const nextOffset = state.offset + LIMIT;
      return {
        ...state,
        images: Object.assign(state.images, {current: state.images.all.slice(state.offset, nextOffset)}),
        offset: nextOffset
      };
    case actions.PREV:
      const sliceFrom = Math.max(0, state.offset - 2 * LIMIT);
      const sliceTo = Math.max(LIMIT, state.offset - LIMIT);
      return {
        ...state,
        images: Object.assign(state.images, {current: state.images.all.slice(sliceFrom, sliceTo)}),
        offset: sliceTo
      };
    case actions.FETCH_IMAGES_STARTED:
      return {
        ...state,
        fetch: {images: {started: true, succeeded: false, failed: false}}
      };
    case actions.FETCH_IMAGES_SUCCEEDED:
      return {
        ...state,
        //images: {all: action.payload, count: action.payload.length, current: action.payload.slice(0, LIMIT)},
        images: Object.assign(state.images, {all: action.payload, current: action.payload.slice(0, LIMIT)}),
        offset: LIMIT,
        fetch: {images: {started: false, succeeded: true, failed: false}}
      };
    case actions.FETCH_IMAGES_FAILED:
      return {
        ...state,
        fetch: {images: {started: false, succeeded: false, failed: true}}
      };
    case actions.FILTER:
      if (state.images.all.length === 0) {
        return state;
      }
      const value = action.payload.trim();

      if (value === '') {
        return {
          ...state,
          images: Object.assign(state.images, {filtered: []}),
        };
      }
      return {
        ...state,
        images: Object.assign(state.images, {filtered: state.images.all.filter((i) => i.name.includes(value))}),
      };
    default:
      return state;
  }
};

export default gallery;