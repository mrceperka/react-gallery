import {AppContainer} from "react-hot-loader";
import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import configureStore from "./utils/configureStore";

const store = configureStore();

// window.mrceperka_gallery_config = {
//   instances: [
//     {
//       id: 'root',
//       lang: 'cs_CZ',
//       filterEnabled: true,
//       image: {
//         data: [
//           {
//             name: 'data-clipboard-text',
//             prefix: '![alt text](',
//             key: 'src',
//             suffix: ')'
//           }
//         ]
//       },
//       fetch: {
//         url: 'http://api.local?type=gallery',
//         atStart: true,
//         options: {credentials: 'same-origin'}
//       }
//     }
//   ],
//   translations: {
//     cs_CZ: {
//       button: {
//         next: 'Další',
//         prev: 'Předchozí',
//         fetch: 'Načíst'
//       },
//       messages : {
//         fetch: {
//           success: 'Načteno, načíst znovu',
//           failed: 'Chyba, zkusit znovu',
//           fetching: 'Načítám'
//         },
//       },
//       images: {
//         def: "Obrázky",
//         filtered: "Výsledky vyhledávání"
//       },
//     }
//   }
// };
const config = window.mrceperka_gallery_config;

config.instances.map((cfg) => {
  ReactDOM.render(
      <AppContainer>
        <App store={store} config={cfg} translations={config.translations[cfg.lang]}/>
      </AppContainer>,
      document.getElementById(cfg.id)
  );
});


if (module.hot) {
  module.hot.accept('./components/App', () => {
    // If you use Webpack 2 in ES modules mode, you can
    // use <App /> here rather than require() a <NextApp />.
    const NextApp = require('./components/App').default;
    config.ids.map((id) => {
      ReactDOM.render(
          <AppContainer>
            <NextApp store={store} config={config}/>
          </AppContainer>,
          document.getElementById(id)
      );
    });
  });
}