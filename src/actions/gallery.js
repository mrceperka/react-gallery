import * as actions from "../constants/actions";

export function next() {
  return {type: actions.NEXT};
}

export function prev() {
  return {type: actions.PREV};
}

export function filter(value) {
  return {type: actions.FILTER, payload: value};
}

export function fetchImages(url, options = {}) {
  return function (dispatch) {
    dispatch({type: actions.FETCH_IMAGES_STARTED});
    return fetch(url, options)
        .then(
            response => response.json()
        ).then(
            json => dispatch({type: actions.FETCH_IMAGES_SUCCEEDED, payload: json})
        )
        .catch(() => dispatch({type: actions.FETCH_IMAGES_FAILED}))
  }
}